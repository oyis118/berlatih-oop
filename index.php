<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>    
<?php 
    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $animal = new animal('shaun');
    echo "Nama Hewan : $animal->name <br>";
    echo "Jumlah Kaki : $animal->legs<br>";
    echo "Berdarah Dingin : $animal->cold_blooded<br><br>";

    $frog = new Frog('buduk');
    echo "Nama Hewan : $frog->name <br>";
    echo "Jumlah Kaki : $frog->legs<br>";
    echo "Berdarah Dingin : $frog->cold_blooded<br>";
    echo $frog->jump()."<br><br>";

    $ape = new Ape('kera sakti');
    echo "Nama Hewan : $ape->name <br>";
    echo "Jumlah Kaki : $ape->legs<br>";
    echo "Berdarah Dingin : $ape->cold_blooded<br>";
    echo $ape->yell();
?>
</body>
</html>